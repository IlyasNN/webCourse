import React from "react"
import Typography from '@material-ui/core/Typography';
import TodoForm from './Components/TodoForm';
import TodoList from './Components/TodoList';
import useTodoState from './Components/useTodoState';
import './styles.css';

const App = () => {
  const { todos, addTodo, deleteNote } = useTodoState([]);

  return (
    <div className="App">
      <div class="header">
        <h1>Todo list</h1>
      </div>

      <TodoForm
        saveTodo={todoText => {
          const trimmedText = todoText.trim();

          if (trimmedText.length > 0) {
            addTodo(trimmedText);
          }
        }}
      />

      <TodoList todos={todos} deleteNote={deleteNote} />
    </div>
  );
};

export default App