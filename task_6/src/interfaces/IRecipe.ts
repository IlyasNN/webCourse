import IIngredient from "./IIngredient";

export default interface IRecipe {
  name: string;
  description: string;
  recipe: string;
  ingredients: Array<IIngredient>;
  image: string;
}
