import IIngredient from "./IIngredient";

export default interface ICart {
  ingredient: IIngredient;
  count: number;
}
