import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
import IRecipe from "../../interfaces/IRecipe";

function getMock() {
  return [
    {
      name: "Творожная запеканка",
      description:
        "Очень быстрая и вкусная творожная запеканка! Очень простой рецепт запеканки с манкой.",
      recipe:
        "1. Перемешиваем творог с яйцами. 2. Добавляем сахар, перемешиваем. 3. Добавляем манку, тщательно перемешиваем. 4. Даём настояться минут 20. 5. Выпекаем творожную запеканку в разогретой духовке при 180 градусах 30 минут.",
      ingredients: [
        { name: "Творог" },
        { name: "Яйцо" },
        { name: "Сахар" },
        { name: "Манка" },
      ],
      image:
        "https://klopotenko.com/wp-content/uploads/2020/04/Tvorozhnaya-zapekanka_siteWeb.jpg?v=1586531960",
    },
    {
      name: "Жареные пельмени",
      description:
        "Жареные пельмени с сыром и сметаной на сковороде из тик тока рецепт простой и быстрый. Сметанно-сырный соус с пельменями невероятно вкусный.",
      recipe:
        "1. На разогретой сковороде растапливаем 50 грамм сливочного масла, добавляем замороженные пельмени и обжариваем 5-7 минут. 2. Соль и черный перец добавляем по вкусу, также можно добавить любимые специи.",
      ingredients: [
        { name: "Сметанно-сырный соус" },
        { name: "Специи" },
        { name: "Пельмени(1кг)" },
      ],
      image:
        "https://static.1000.menu/img/content-v2/ce/13/21484/pelmeni-jarenye-s-syrom-na-skovorode_1632645927_9_max.jpg",
    },
    {
      name: "Окрошка",
      description:
        "Тонкие блины на молоке — это английский вариант традиционных пышных русских блинов, выпеченных на дрожжах. ",
      recipe:
        "1. Порезать овощи. 2. Смешать в тарелке. 3. Залить квасом. 4. Посолить.",
      ingredients: [
        { name: "Молоко" },
        { name: "Яйцо" },
        { name: "Мука" },
        { name: "Масло сливочное" },
        { name: "Сахар" },
        { name: "Соль" },
      ],
      image:
        "https://s1.eda.ru/StaticContent/Photos/140424040926/140429194401/p_O.jpg",
    },
    {
      name: "Блины",
      description:
        "Классическая окрошка на квасе с колбасой и редисом — традиционное блюдо жарким летом",
      recipe:
        "1. Смешайте яйца, соль, сахар и размешайте миксером. 2. Введите муку и влейте молоко. 3. ДВзбейте блинное тесто, чтобы добиться однородной консистенции. 4. Налейте в центр сковородки небольшую порцию теста.",
      ingredients: [
        { name: "Квас" },
        { name: "Вареная колбаса" },
        { name: "Картофель" },
        { name: "Огурцы" },
        { name: "Яйцо" },
        { name: "Зелень" },
        { name: "Cоль" },
      ],
      image:
        "https://prostokvashino.ru/upload/iblock/d62/d6269fa359b8595e632183c2c267c4ec.jpg",
    },
  ] as IRecipe[];
}

@Injectable({
  providedIn: "root",
})
export class RecipesStoreService {
  private _recipes: BehaviorSubject<IRecipe[]> = new BehaviorSubject(
    [] as IRecipe[]
  );
  public readonly recipes: Observable<IRecipe[]> = this._recipes.asObservable();
  constructor() {
    this.fetchData();
  }

  fetchData() {
    this._recipes.next(getMock());
  }
}
