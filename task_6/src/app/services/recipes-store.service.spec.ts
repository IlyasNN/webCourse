import { TestBed } from "@angular/core/testing";

import { RecipesStoreService } from "./recipes-store.service";

describe("ProductStoreService", () => {
  let service: RecipesStoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RecipesStoreService);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });
});
