import { Injectable } from "@angular/core";
import IRecipe from "src/interfaces/IRecipe";
import { BehaviorSubject, Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class RecipeService {
  private _recipe: BehaviorSubject<IRecipe> = new BehaviorSubject(
    {} as IRecipe
  );
  public readonly recipe: Observable<IRecipe> = this._recipe.asObservable();

  constructor() {}

  updateCurrentIngredients(currentProduct: IRecipe) {
    this._recipe.next(currentProduct);
  }
}
