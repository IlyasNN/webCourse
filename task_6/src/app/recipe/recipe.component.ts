import { Component, OnInit } from "@angular/core";
import IIngredient from "src/interfaces/IIngredient";
import IRecipe from "src/interfaces/IRecipe";
import { CartStoreService } from "../services/cart-store.service";
import { RecipeService } from "../services/recipe.service";
import { NotificationsService } from "angular2-notifications";

@Component({
  selector: "app-recipe",
  templateUrl: "./recipe.component.html",
  styleUrls: ["./recipe.component.css"],
})
export class RecipeComponent implements OnInit {
  public recipe: IRecipe = {
    name: "",
    description: "",
    recipe: "",
    ingredients: [],
    image: "",
  };

  constructor(
    private recipeStore: RecipeService,
    private cartStore: CartStoreService,
    private notificationService: NotificationsService
  ) {}

  ngOnInit(): void {
    this.recipeStore.recipe.subscribe(
      (recipe: IRecipe) => (this.recipe = recipe)
    );
  }

  onAdd(ingredient: IIngredient) {
    this.cartStore.addIngredientIntoCart(ingredient);
    this.notificationService.success(
      "Success",
      "Вы успешно добавили ингредиент",
      {
        position: ["bottom", "right"],
        timeOut: 500,
        animate: "fade",
      }
    );
  }

  onAddAll(recipe: IRecipe) {
    for (const ingredient of recipe.ingredients) {
      this.cartStore.addIngredientIntoCart(ingredient);
    }
    this.notificationService.success(
      "Success",
      "Вы успешно добавили ингредиенты",
      {
        position: ["bottom", "right"],
        timeOut: 500,
        animate: "fade",
      }
    );
  }
}
