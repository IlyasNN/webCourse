import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import IRecipe from "../../interfaces/IRecipe";
import { RecipesStoreService } from "../services/recipes-store.service";
import { RecipeService } from "../services/recipe.service";

@Component({
  selector: "app-recipes-list",
  templateUrl: "./recipes-list.component.html",
  styleUrls: ["./recipes-list.component.css"],
})
export class RecipesListComponent implements OnInit {
  public recipes: Array<IRecipe> = [];
  public recipe: IRecipe[] = [];
  constructor(
    private receiptsStore: RecipesStoreService,
    private receiptStore: RecipeService
  ) {}

  ngOnInit(): void {
    this.receiptsStore.recipes.subscribe(
      (recipes: IRecipe[]) => (this.recipes = recipes)
    );
  }

  onClick(receipt: IRecipe) {
    this.receiptStore.updateCurrentIngredients(receipt);
  }
}
