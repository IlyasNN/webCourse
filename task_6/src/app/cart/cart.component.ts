import { Component, OnInit } from "@angular/core";
import { CartStoreService } from "../services/cart-store.service";
import ICart from "../../interfaces/ICart";
import IIngredient from "../../interfaces/IIngredient";

@Component({
  selector: "app-cart-page",
  templateUrl: "./cart.component.html",
  styleUrls: ["./cart.component.css"],
})
export class CartComponent implements OnInit {
  public cart: ICart[] = [];
  constructor(private cartStore: CartStoreService) {}

  ngOnInit(): void {
    this.cartStore.cart.subscribe((cart) => (this.cart = cart));
  }

  onAddIngredient(ingredient: IIngredient): void {
    this.cartStore.addIngredientIntoCart(ingredient);
  }

  onDeleteIngredient(ingredient: IIngredient): void {
    this.cartStore.deleteIngredientFromCart(ingredient);
  }
}
