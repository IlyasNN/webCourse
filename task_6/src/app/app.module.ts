import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { RecipesListComponent } from "./recipes-list/recipes-list.component";
import { RecipeInformationComponent } from "./recipe-information/recipe-information.component";
import { CartComponent } from "./cart/cart.component";
import { NotFoundStubComponent } from "./not-found-stub/not-found-stub.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MatCardModule } from "@angular/material/card";
import { HeaderComponent } from "./header/header.component";
import { MatButtonModule } from "@angular/material/button";
import { MatIconModule } from "@angular/material/icon";
import { RecipeComponent } from "./recipe/recipe.component";
import { SimpleNotificationsModule } from "angular2-notifications";

@NgModule({
  declarations: [
    AppComponent,
    RecipesListComponent,
    RecipeInformationComponent,
    CartComponent,
    NotFoundStubComponent,
    HeaderComponent,
    RecipeComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule,
    SimpleNotificationsModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
